package loreipsum;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {
    @FindBy(xpath = "//a[contains(@href,'ru')]")
    private WebElement langRusButton;

    @FindBy(xpath = "//h2[contains(text(),'Lorem Ipsum?')]/parent::div/child::p")
    private WebElement paragraph;

    @FindBy (xpath = "//input[@name='what']")
    private List<WebElement> radioButtons;

    @FindBy(xpath = "//input[@id='start']")
    private WebElement checkBox;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement amountInput;

    @FindBy(xpath = "//input[@id='generate']")
    private WebElement generateButton;

    @FindBy(xpath = "//div[@id='lipsum']//p")
    private List<WebElement> paragraphsOnGeneratePage;

    @FindBy(xpath = "//div[@id='generated']")
    private WebElement generatedDiv;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickLangRusButton(){
        langRusButton.click();
    }

    public String paragraphText(){return paragraph.getText();}

    public void clickGenerateButton(){generateButton.click();}

    public WebElement getGenerateButton(){return generateButton;}

    public String paragraphText1(int ind){return paragraphsOnGeneratePage.get(ind).getText();}

    public WebElement getRadioButton(int ind){return radioButtons.get(ind);}

    public void clickRadioButton(int ind){
        getRadioButton(ind).click();}

    public void searchByKeyword(final String keyword) {
        amountInput.clear();
        amountInput.sendKeys(keyword);
        clickGenerateButton();
    }

    public String getGeneratedDivText(){return generatedDiv.getText();}

    public void clickCheckBox(){checkBox.click();}

}
