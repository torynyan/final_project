package bbc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SportPageBBC extends BasePageBBC{

    @FindBy(xpath = "//div[@role='menubar']//a[@data-stat-title='Football']")
    private WebElement menuBarFootball;

    @FindBy(xpath = "//div[@class='gel-wrap']//a[@data-stat-title='Scores & Fixtures']")
    private WebElement scoresAndFix;

    @FindBy(xpath = "//input[@class='sp-c-search__input gel-1/1']")
    private WebElement inputSearch;

    @FindBy(xpath = "//a[@class='sp-c-search__result-item']")
    private List<WebElement> listOfSearchResult;

    @FindBy(xpath = "//h1[@id='page']")
    private WebElement titleScoresAndFix;

    @FindBy(xpath = "//a[contains(@href,'filter=results')]")
    private WebElement resultsForAMonth;

    @FindBy(xpath = "//article[@data-event-id='EFBO2209378']//span[@data-team-id='TFBB327']")
    private WebElement firstTeamResult;

    @FindBy(xpath = "//ul[contains(@class,'gs-o-list-ui')]//span[contains(@class,'gs-u-display-none')]")
    private List<WebElement> nameOfTeam;


    @FindBy(xpath = "//ul[contains(@class,'gs-o-list-ui')]//span[contains(@class,'sp-c-fixture__number')]")
    private List<WebElement> scoreOfTeam;




    @FindBy(xpath = "//div[@class='football-oppm-header']")
    private WebElement centerOfScreen;

    @FindBy(xpath = "//div[@class='football-oppm-header']//span[contains(@class,'sp-c-fixture__number')]")
    private List<WebElement> scoreOfTeamInCenter;

    @FindBy(xpath = "//div[@class='football-oppm-header']//span[contains(@class,'gs-u-display-none')]")
    private List<WebElement> nameOfTeamInCenter;


    public SportPageBBC(WebDriver driver) {
        super(driver);
    }

    public void clickMenuBarFootball(){menuBarFootball.click();}

    public WebElement getMenuBarFootball(){return menuBarFootball;}

    public void clickScoresAndFix(){scoresAndFix.click();}

    public WebElement getScoresAndFix(){return scoresAndFix;}

    public WebElement getInputSearch(){return inputSearch;}

    public void searchChampionship(final String keyword) {
        inputSearch.sendKeys(keyword);
        listOfSearchResult.get(0).click();
    }

    public String getTitleScoresAndFixText(){return titleScoresAndFix.getText();}

    public void clickResultsForAMonth(){resultsForAMonth.click();}

    public WebElement getFirstTeamResult(){return firstTeamResult;}

    public String getNameTeamText(int ind){return nameOfTeam.get(ind).getText();}

    public void clickNameOfTeam(int ind){
        nameOfTeam.get(ind).click();}

    public String getScoreOfTeamText(int ind){return scoreOfTeam.get(ind).getText();}

    public WebElement getCenterOfScreen(){return centerOfScreen;}


    public String getScoreOfTeamInCenterText(int ind){return scoreOfTeamInCenter.get(ind).getText();}

    public String getNameOfTeamInCenterText(int ind){return nameOfTeamInCenter.get(ind).getText();}



}
