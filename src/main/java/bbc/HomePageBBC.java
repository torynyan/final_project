package bbc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageBBC extends BasePageBBC {

    @FindBy (xpath = "//header//li[@class='orb-nav-newsdotcom']")
    private WebElement newsButton;

    @FindBy (xpath = "//header//li[@class='orb-nav-sport']")
    private WebElement sportButton;

    public HomePageBBC(WebDriver driver) {
        super(driver);
    }

    public void clickNewsButton(){newsButton.click();}
    public void clickSportButton(){sportButton.click();}
}
