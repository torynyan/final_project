package bbc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class NewsPageBBC extends BasePageBBC {

    @FindBy(xpath = "//button[@class='tp-close tp-active']")
    private WebElement closePopupButton;

    @FindBy(xpath = "//div[@class='gs-c-promo-body gs-u-display-none gs-u-display-inline-block@m gs-u-mt@xs gs-u-mt0@m gel-1/3@m']//h3")
    private WebElement headlineArticle;

    @FindBy (xpath = "//div[@id='news-top-stories-container']//a[@class='gs-c-promo-heading gs-o-faux-block-link__overlay-link gel-pica-bold nw-o-link-split__anchor']//h3[@class='gs-c-promo-heading__title gel-pica-bold nw-o-link-split__text']")
    private List<WebElement> secondaryArticleTitles;

    @FindBy (xpath = "//li[contains(@class,'gs-u-display-block')]//a[@href='/news/science-environment-56837908']//span")
    private WebElement categoryLinkOfClimate;

    @FindBy (xpath = "//li[contains(@class,'gs-u-display-block')]//span[text()='Coronavirus']")
    private WebElement categoryLinkOfCoronavirus;

    @FindBy (xpath = "//a[@href='/news/10725415']")
    private WebElement linkHowToShareWithBBC;

    @FindBy (xpath = "//div[@class='embed-content-container']")
    private WebElement searchForm;

    @FindBy(xpath = "//textarea[@class='text-input--long']")
    private WebElement textareaForm;

    @FindBy(xpath = "//input[@class='text-input__input']")
    private List<WebElement> inputsOfContactInfo;

    @FindBy(xpath = "//input[@type='checkbox']")
    private List<WebElement> checkBoxes;

    @FindBy(xpath = "//button[@class='button']")
    private WebElement submitButton;

    @FindBy(xpath = "//div [@class='input-error-message']")
    private WebElement errorMessage;

    @FindBy (xpath = "//li[contains(@class,'gel-long-primer')]//span[text()='Your Coronavirus Stories']")
    private WebElement spanYourCorStories;

    @FindBy(xpath = "//input[@id='orb-search-q']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[@id='orb-search-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//p[@class='ssrcss-6arcww-PromoHeadline e1f5wbog4']//span")
    private List<WebElement> articlesOfSearchWord;

    public NewsPageBBC(WebDriver driver) {
        super(driver);
    }

    public void clickClosePopupButton() {
        closePopupButton.click();
    }

    public WebElement getClosePopupButton() {
        return closePopupButton;
    }

    public String getHeadlineArticleText() {
       return headlineArticle.getText();
    }

    public String getSecondaryArticleTitleText(int ind){return secondaryArticleTitles.get(ind).getText();}

    public String getCategoryLinkOfClimateText(){return categoryLinkOfClimate.getText();}

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword);
        searchButton.click();
    }

    public String getArticleOfSearchWordText(int ind){return articlesOfSearchWord.get(ind).getText();}

    public void clickCategoryLinkOfCoronavirus(){categoryLinkOfCoronavirus.click();}

    public void clickSpanYourCorStories(){spanYourCorStories.click();}

    public void clickLinkHowToShareWithBBC(){linkHowToShareWithBBC.click();}

    public WebElement getSearchForm(){return searchForm;}

    public void enterTextInTextarea(final String story) {
        textareaForm.sendKeys(story);
    }
    public void enterTextInNeedInput(int ind, final String needInfo) {
        inputsOfContactInfo.get(ind).sendKeys(needInfo);
    }

    public void clickCheckBox(int ind){checkBoxes.get(ind).click();}

    public void clickSubmitButton(){submitButton.click();}

    public boolean errorMessageVisible(){return errorMessage.isDisplayed();}
    public WebElement getErrorMessage(){return errorMessage;}
}