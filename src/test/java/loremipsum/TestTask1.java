package loremipsum;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestTask1 extends BaseTest{
    private static final long DEFAULT_TIMEOUT = 120;

    @Test
    public void checkthatTheWordCorrectlyAppearsInTheFirstParagraph() {
        getHomePage(). clickLangRusButton();
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().paragraphText().contains("рыба"));

    }

    @Test
    public void checkThatDefaultSettingResultInTextStartingWithLoremIpsum(){
        getHomePage().clickGenerateButton();
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().paragraphText1(0).startsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));
    }
}
