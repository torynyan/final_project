package loremipsum;

import org.testng.annotations.Test;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

public class TestTask2 extends BaseTest{
    private static final long DEFAULT_TIMEOUT = 120;

    @Test
    public void checkTheResultHasExpectedWords(){
        getHomePage().clickRadioButton(1);
        getHomePage().searchByKeyword("10");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 10 words"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(1);
        getHomePage().searchByKeyword("-1");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 words"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(1);
        getHomePage().searchByKeyword("0");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 words"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(1);
        getHomePage().searchByKeyword("5");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 words"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(1);
        getHomePage().searchByKeyword("20");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 20 words"));

    }

    @Test
    public void checkTheResultHasExpectedBytes(){
        getHomePage().clickRadioButton(2);
        getHomePage().searchByKeyword("10");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 10 bytes"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(2);
        getHomePage().searchByKeyword("-1");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 bytes"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(2);
        getHomePage().searchByKeyword("0");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 bytes"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(2);
        getHomePage().searchByKeyword("5");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 5 bytes"));
        getHomePage().backPreviousPage();
        getHomePage().clickRadioButton(2);
        getHomePage().searchByKeyword("20");
        getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getHomePage().getGeneratedDivText().contains(" 20 bytes"));
    }


    @Test
    public void checkThatResultNoLongerStartsWithLoremIpsum(){
        getHomePage().clickCheckBox();
        getHomePage().clickGenerateButton();
        assertFalse(getHomePage().paragraphText1(0).startsWith("Lorem ipsum dolor sit amet"));
    }
    @Test
    public void checkThatRandomlyGeneratedTextParagraphsContainTheWord(){
        int result=0;
        for (int i = 0; i < 10; i++) {
            getHomePage().waitVisibilityOfElement(DEFAULT_TIMEOUT,getHomePage().getGenerateButton());
            getHomePage().clickGenerateButton();
            getHomePage().waitForPageLoadComplete(DEFAULT_TIMEOUT);
            for (int j = 0; j < 5; j++) {
                if(getHomePage().paragraphText1(j).contains("lorem")){
                    result ++;
                }
            }
            getHomePage().backPreviousPage();
        }
         result/=10;
        assertTrue(result >= 2);

    }
}
