package bbc;

import org.testng.annotations.Test;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertTrue;

public class TestTask2BBC2 extends BaseTestBBC{
    private static final long DEFAULT_TIMEOUT = 60;

    @Test
    public void checkThatTeamScoresDisplayCorrectly1() {

        String championship ="Scottish Championship";
        String firstTeamName = "Ayr United";
        String firstTeamScore = "0";
        String secondTeamName = "Greenock Morton";
        String secondTeamScore = "2";

        getHomePageBBC().clickSportButton();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getMenuBarFootball());
        getSportPageBBC().clickMenuBarFootball();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getScoresAndFix());
        getSportPageBBC().clickScoresAndFix();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getInputSearch());
        getSportPageBBC().searchChampionship(championship);
        assertTrue(getSportPageBBC().getTitleScoresAndFixText().contains(championship));
        getSportPageBBC().clickResultsForAMonth();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getFirstTeamResult());

        assertTrue(getSportPageBBC().getNameTeamText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameTeamText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(1).contains(secondTeamScore));
        getSportPageBBC().clickNameOfTeam(0);
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getCenterOfScreen());
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(1).contains(secondTeamScore));

    }
    @Test
    public void checkThatTeamScoresDisplayCorrectly2() {

        String championship ="Scottish Championship";
        String firstTeamName = "Dunfermline";
        String firstTeamScore = "1";
        String secondTeamName = "Hamilton Academical";
        String secondTeamScore = "0";

        getHomePageBBC().clickSportButton();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getMenuBarFootball());
        getSportPageBBC().clickMenuBarFootball();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getScoresAndFix());
        getSportPageBBC().clickScoresAndFix();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getInputSearch());
        getSportPageBBC().searchChampionship(championship);
        assertTrue(getSportPageBBC().getTitleScoresAndFixText().contains(championship));
        getSportPageBBC().clickResultsForAMonth();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getFirstTeamResult());

        assertTrue(getSportPageBBC().getNameTeamText(2).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(2).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameTeamText(3).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(3).contains(secondTeamScore));
        getSportPageBBC().clickNameOfTeam(2);
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getCenterOfScreen());
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(1).contains(secondTeamScore));

    }
    @Test
    public void checkThatTeamScoresDisplayCorrectly3() {

        String championship ="Scottish Championship";
        String firstTeamName = "Inverness Caledonian Thistle";
        String firstTeamScore = "2";
        String secondTeamName = "Queen of the South";
        String secondTeamScore = "2";

        getHomePageBBC().clickSportButton();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getMenuBarFootball());
        getSportPageBBC().clickMenuBarFootball();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getScoresAndFix());
        getSportPageBBC().clickScoresAndFix();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getInputSearch());
        getSportPageBBC().searchChampionship(championship);
        assertTrue(getSportPageBBC().getTitleScoresAndFixText().contains(championship));
        getSportPageBBC().clickResultsForAMonth();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getFirstTeamResult());

        assertTrue(getSportPageBBC().getNameTeamText(4).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(4).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameTeamText(5).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(5).contains(secondTeamScore));
        getSportPageBBC().clickNameOfTeam(4);
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getCenterOfScreen());
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(1).contains(secondTeamScore));

    }
    @Test
    public void checkThatTeamScoresDisplayCorrectly4() {

        String championship ="Scottish Championship";
        String firstTeamName = "Raith Rovers";
        String firstTeamScore = "1";
        String secondTeamName = "Arbroath";
        String secondTeamScore = "2";

        getHomePageBBC().clickSportButton();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getMenuBarFootball());
        getSportPageBBC().clickMenuBarFootball();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getScoresAndFix());
        getSportPageBBC().clickScoresAndFix();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getInputSearch());
        getSportPageBBC().searchChampionship(championship);
        assertTrue(getSportPageBBC().getTitleScoresAndFixText().contains(championship));
        getSportPageBBC().clickResultsForAMonth();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getFirstTeamResult());

        assertTrue(getSportPageBBC().getNameTeamText(6).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(6).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameTeamText(7).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(7).contains(secondTeamScore));
        getSportPageBBC().clickNameOfTeam(6);
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getCenterOfScreen());
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(1).contains(secondTeamScore));

    }
    @Test
    public void checkThatTeamScoresDisplayCorrectly5() {

        String championship ="Scottish Championship";
        String firstTeamName = "Partick Thistle";
        String firstTeamScore = "1";
        String secondTeamName = "Kilmarnock";
        String secondTeamScore = "1";

        getHomePageBBC().clickSportButton();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getMenuBarFootball());
        getSportPageBBC().clickMenuBarFootball();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getScoresAndFix());
        getSportPageBBC().clickScoresAndFix();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getInputSearch());
        getSportPageBBC().searchChampionship(championship);
        assertTrue(getSportPageBBC().getTitleScoresAndFixText().contains(championship));
        getSportPageBBC().clickResultsForAMonth();
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getFirstTeamResult());

        assertTrue(getSportPageBBC().getNameTeamText(8).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(8).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameTeamText(9).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamText(9).contains(secondTeamScore));
        getSportPageBBC().clickNameOfTeam(8);
        getSportPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getSportPageBBC().getCenterOfScreen());
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(0).contains(firstTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(0).contains(firstTeamScore));
        assertTrue(getSportPageBBC().getNameOfTeamInCenterText(1).contains(secondTeamName));
        assertTrue(getSportPageBBC().getScoreOfTeamInCenterText(1).contains(secondTeamScore));

    }

}
