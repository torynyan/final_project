package bbc;

import org.testng.annotations.Test;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertTrue;

public class TestTask1BBC extends BaseTestBBC {
    private static final long DEFAULT_TIMEOUT = 60;
    @Test
    public void checkTheNameOfTheHeadlineArticle(){
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        assertTrue(getNewsPageBBC().getHeadlineArticleText().contains("Anxious wait for news after tsunami cuts off Tonga"));
    }
    @Test
    public void checksSecondaryArticleTitles(){
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(0).contains("British woman died in Tonga tsunami, says brother"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(1).contains("Rabbi describes escape from Texas synagogue siege"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(2).contains("Suspect identified over Anne Frank's betrayal"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(3).contains("French far-right candidate guilty of hate speech"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(4).contains("Djokovic back in Serbia after Australia deportation"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(5).contains("Wealth of 10 richest men 'doubled in pandemic'"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(6).contains("BBC licence fee to be frozen at £159 for two years"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(7).contains("The Voice Holland suspended over misconduct claims"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(8).contains("Beijing urges end to foreign deliveries"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(9).contains("Armed forces to take over Channel migrant operations"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(10).contains("Beijing urges end to foreign deliveries over Covid"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(11).contains("Armed forces to take over Channel migrant operations"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(12).contains("No general ticket sale for Beijing Olympics"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(13).contains("Nasa fixes megarocket equipment glitch"));
        assertTrue(getNewsPageBBC().getSecondaryArticleTitleText(14).contains("Credit Suisse boss resigns over Covid breaches"));
    }

    @Test
    public void checkTheNameOfTheFirstArticleAgainstASpecifiedValue() {
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        getNewsPageBBC().searchByKeyword(getNewsPageBBC().getCategoryLinkOfClimateText());
        assertTrue(getNewsPageBBC().getArticleOfSearchWordText(0).contains("Climate Tales"));
    }
}
