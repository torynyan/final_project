package bbc;

import org.testng.annotations.Test;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestTask2BBC1 extends BaseTestBBC{
    private static final long DEFAULT_TIMEOUT = 60;
    @Test
    public void checkThatUserCanNotSubmitQuestionWithInvalidEmail()  {
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        getNewsPageBBC().clickCategoryLinkOfCoronavirus();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickSpanYourCorStories();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickLinkHowToShareWithBBC();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getSearchForm());
        getNewsPageBBC().enterTextInTextarea("It’s a fun game with high quality pixel art animation and inventive level design and gameplay modifiers that offer up a new twist to the gameplay with each level.");
        getNewsPageBBC().enterTextInNeedInput(0,"Victoria");
        getNewsPageBBC().enterTextInNeedInput(1,"torynyangmail.com");
        getNewsPageBBC().enterTextInNeedInput(2,"+380636284047");
        getNewsPageBBC().enterTextInNeedInput(3,"Kyiv");
        getNewsPageBBC().clickCheckBox(0);
        getNewsPageBBC().clickCheckBox(1);
        getNewsPageBBC().clickSubmitButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getErrorMessage());
        assertTrue(getNewsPageBBC().errorMessageVisible());
    }
    @Test
    public void checkThatUserCanNotSubmitQuestionWithoutName()  {
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        getNewsPageBBC().clickCategoryLinkOfCoronavirus();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickSpanYourCorStories();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickLinkHowToShareWithBBC();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getSearchForm());
        getNewsPageBBC().enterTextInTextarea("It’s a fun game with high quality pixel art animation and inventive level design and gameplay modifiers that offer up a new twist to the gameplay with each level.");
        getNewsPageBBC().enterTextInNeedInput(1,"torynyan@gmail.com");
        getNewsPageBBC().enterTextInNeedInput(2,"+380636284047");
        getNewsPageBBC().enterTextInNeedInput(3,"Kyiv");
        getNewsPageBBC().clickCheckBox(0);
        getNewsPageBBC().clickCheckBox(1);
        getNewsPageBBC().clickSubmitButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getErrorMessage());
        assertTrue(getNewsPageBBC().errorMessageVisible());
    }
    @Test
    public void checkThatUserCanNotSubmitQuestionWithNotCheckedAcceptBox()  {
        getHomePageBBC().clickNewsButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getClosePopupButton());
        getNewsPageBBC().clickClosePopupButton();
        getNewsPageBBC().clickCategoryLinkOfCoronavirus();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickSpanYourCorStories();
        getNewsPageBBC().waitForPageLoadComplete(DEFAULT_TIMEOUT);
        getNewsPageBBC().clickLinkHowToShareWithBBC();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getSearchForm());
        getNewsPageBBC().enterTextInTextarea("It’s a fun game with high quality pixel art animation and inventive level design and gameplay modifiers that offer up a new twist to the gameplay with each level.");
        getNewsPageBBC().enterTextInNeedInput(0,"Victoria");
        getNewsPageBBC().enterTextInNeedInput(1,"torynyan@gmail.com");
        getNewsPageBBC().enterTextInNeedInput(2,"+380636284047");
        getNewsPageBBC().enterTextInNeedInput(3,"Kyiv");
        getNewsPageBBC().clickCheckBox(0);
        getNewsPageBBC().clickSubmitButton();
        getNewsPageBBC().waitVisibilityOfElement(DEFAULT_TIMEOUT,getNewsPageBBC().getErrorMessage());
        assertTrue(getNewsPageBBC().errorMessageVisible());
    }
}
